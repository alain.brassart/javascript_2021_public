# Les fonctions en JavaScript

> * Auteur : Alain BRASSART
> * Date de publication : 07/03/2021
> * OS: Windows 10 (version 20H2)
> * VS Code : version 1.53.2 (system setup)
> * Chrome : version 87.0.4280.141

![CC-BY-NC-SA](../img/cc-by-nc-sa-150.png) [Licence Creative Commons](https://creativecommons.org/licenses/by-nc-sa/3.0/fr/)

## Sommaire

  - [1 Présentation](#1-présentation)
    - [1.1 Déclarer une fonction en JavaScript](#11-déclarer-une-fonction-en-javascript)
    - [1.2 Appeler une fonction](#12-appeler-une-fonction)
    - [1.3 Portée des variables](#13-portée-des-variables)
    - [1.4 Fonctions imbriquées](#14-fonctions-imbriquées)
  - [Activité \: Codage de fonctions pour calculer et interpréter l'IMC](#activité-codage-de-fonctions-pour-calculer-et-interpréter-limc)
  - [2. Fonctions anonymes](#2-fonctions-anonymes)
    - [2.1 Déclarer une fonction anonyme](#21-déclarer-une-fonction-anonyme)
    - [2.2 Exécuter une fonction anonyme en utilisant une variable](#22-exécuter-une-fonction-anonyme-en-utilisant-une-variable)
    - [2.3 Auto-invoquer une fonction anonyme](#23-auto-invoquer-une-fonction-anonyme)
    - [2.4 Exécuter une fonction anonyme lors du déclenchement d'un évènement](#24-exécuter-une-fonction-anonyme-lors-du-déclenchement-dun-évènement)
  - [Exercices](#exercices)
    - [Exercice 1 : Calcul du temps de parcours d'un trajet](#exercice-1-calcul-du-temps-de-parcours-dun-trajet)
    - [Exercice 2 : Recherche du nombre de multiples de 3](#exercice-2-recherche-du-nombre-de-multiples-de-3)

## 1 Présentation

De manière générale, en programmation, une fonction correspond à une portion de code réalisant un traitement spécifique qui pourra être appelé ultérieurement dans le reste du programme par une simple ligne. L'utilisation des fonctions permet d'éviter d'écrire plusieurs fois le même code et améliore la lisibilité du programme.  

### 1.1 Déclarer une fonction en JavaScript

Avant tout, une fonction doit être déclarée dans le script où elle sera utilisée. La déclaration d'une fonction s'effectue en utilisant le mot clé <code>function</code> suivi par :

* le nom de la fonction
* la liste des paramètres à passer à la fonction, entre paranthèses et séparés par des virgules, si la fonction n'utilise pas de paramètres, les parenthèses doivent restées vides.
* les instructions correspondant au traitement que doit réaliser la fonction, entre accolades, { }

Le code suivant, par exemple, définit une fonction intitulée <code>calculerSurface</code>  :

        function calculerSurface(prmLongueur,prmLargeur) {
                let surface ;
                surface = prmLongueur * prmLargeur;
                return surface ;
        }

La fonction <code>calculerSurface</code> prend deux paramètres <code>prmLongueur</code> et <code>prmLargeur</code> correspondant à la longueur et à la largeur. Elle est composée des instructions qui permettent de calculer et de retourner la surface définie par ces deux paramètres.

> L'instruction <code>return</code> spécifie la valeur qui est renvoyée par la fonction

### 1.2 Appeler une fonction

Pour exécuter le code d'une fonction, il faut appeler celle-ci en utilisant son nom et en lui spécifiant les valeurs à prendre en compte pour les paramètres. Si la fonction retourne un résultat, il est conseillé d'utiliser une variable pour le récupérer.

Le code suivant permet d'appeler la fonction <code>calculerSurface()</code> : 

        let surface = calculerSurface(4,3);

Cette instruction permet d'appeler la fonction <code>calculerSurface()</code> pour calculer la surface correspondant à une longueur et à une largeur définies par les valeurs 4 et 3. La valeur calculée sera retournée dans la variable <code>surface</code>  

> Il est possible également d'appeler une fonction en passant par des variables pour les paramètres.

        let longueur = 4;
        let largeur = 3;
        let surface = calculerSurface(longueur,largeur);

### 1.3 Portée des variables

La portée d'une variable désigne l'espace du script dans lequel cette variable sera accessible. En JavaScript, il n'existe que deux espaces de portée différents : l'espace **global** et l'espace **local**. De manière simple, l'espace global désigne la totalité d'un script excepté l'intérieur des fonctions, l'espace local désigne, à l'inverse, l'espace dans une fonction.

> Une variable globale sera accessible à travers tout le script y compris depuis une fonction. En revanche, une variable locale ne sera accessible que dans la fonction où elle a été déclarée et ne pourra pas être manipulée depuis l'espace global.

Exemple :

        let a = 5 ;         // a est une variable globale
        test() ;
        
        function test() {
                let b = 3 ;     // b est une variable locale à la fonction test()
                b = b * a ;     // la variable a peut être utilisée dans la fonction
                console.log(b) ;
        }

En terme de qualité de codage, il est impératif de favoriser au maximum l'utilisation des variables locales grâce au passage de paramètres dans les fonctions.

### 1.4 Fonctions imbriquées

En JavaScript, il est possible de définir dans une fonction d'autres fonctions qui sont alors dites fonctions internes et pour lesquelles des règles de visibilité s'appliquent.

* une fonction interne a accès aux variables définies dans la fonction qui la contient
* par contre, une fonction n'a pas accès aux variables définies dans ses fonctions internes

Exemple :

        function calculerSurface() {
                // déclaration des variables locales
                let longueur;
                let largeur;
                // déclaration des fonctions internes
                function initialiserLongueur() {        
                        longueur = 4;
                }
                function initialiserLargeur() {
                        largeur = 3;
                }
                // appel des fonctions internes
                initialiserLongueur();
                initialiserLargeur();
                // retour du résultat
                return largeur * longueur;
        }

        let surface = calculerSurface();
        console.log(surface);

Dans cet exemple, les fonctions internes <code>initialiserLongueur()</code> et <code>initialiserLargeur()</code> sont utilisées par la fonction <code>calculerSurface()</code> pour initialiser les valeurs des variables locales <code>longueur</code> et <code>largeur</code>   

***
## Activité \: Codage de fonctions pour calculer et interpréter l'IMC

Reprendre l'application de calcul d'IMC en utilisant :
* une fonction <code>calculerIMC()</code> qui reçoit en paramètres la taille en cm et le poids en kg et qui retourne la valeur de l'IMC correspondante.

        function calculerIMC(prmTaille,prmPoids) {
                let valIMC;
                // codage
                return valIMC;
        }
* une fonction <code>interpreterIMC()</code> qui recoit en paramètre la valeur de l'IMC et qui retourne l'interprétation correspondante.

        function interpreterIMC(prmIMC) {
                let interpretation = "";
                // codage
                return interpretation;
        }

Coder une nouvelle version basée sur l'utilisation des fonctions imbriquées, pour cela :

* prévoir une fonction <code>decrire_corpulence(prmTaille, prmPoids)</code> qui retourne un message contenant la valeur de l'IMC et l'état de corpulence correspondant.
* coder les fonctions <code>calculerIMC()</code> et <code>interpreterIMC()</code> en tant que fonctions internes à la fonction <code>decrire_corpulence(prmTaille, prmPoids)</code>
* les variables représentant l'IMC et l'interprétation de l'IMC seront déclarées en tant que variables locales à la fonction <code>decrire_corpulence()</code> et pourront donc être utilisées directement dans les fonctions internes.

Exemple de résultat attendu :

![Img_IMC_Fcts_Imbr](img/Images_Chapitre_4/Img_IMC_Fcts_Imbr.png)
***

## 2. Fonctions anonymes

### 2.1 Déclarer une fonction anonyme

En JavaScript, il est possible de créer une fonction anonyme de la même manière qu'une fonction classique en utilisant le mot clé <code>function</code> mais sans spécifier de nom pour cette fonction.

Exemple :

        function() {
                console.log("Je suis une fonction anonyme");
        }

Dans cet exemple, on a déclaré une fonction anonyme dont la tâche est d'afficher le message "Je suis une fonction anonyme" dans la console du navigateur.

Le problème est de savoir maintenant comment appeler cette fonction pour qu'elle soit exécutée. Pour cela, ll existe plusieurs possibilités, notamment :

* enfermer le code de la fonction dans une variable et utiliser cette variable comme une fonction
* auto-invoquer la fonction
* utiliser un évènement pour déclencher l'exécution de la fonction

### 2.2 Exécuter une fonction anonyme en utilisant une variable 

Ici le principe consiste à déclarer une variable et à lui affecter une fonction anonyme

Exemple :

        let afficherMessage = function() {
                console.log("Je suis une fonction anonyme");
        }

Dans l'exemple ci-dessus, la variable <code>afficherMessage</code> contient une fonction anonyme qui va afficher le message "Je suis une fonction anonyme".


Pour appeler la fonction anonyme et exécuter son code, il faut utiliser le nom de la variable qui la contient suivi d'un couple de parenthèses. Ceds parenthèses sont des parenthèses dites **appelantes** car elles servent à exécuter la fonction qui les précède.

Exemple :

        let afficherMessage = function() {
                console.log("Je suis une fonction anonyme");
        }

        afficherMessage();      // exécution de la fonction anonyme contenue dans la variable afficherMessage

### 2.3 Auto-invoquer une fonction anonyme 

Une fonction anonyme peut être créée pour s'invoquer (ou s'appeler ou encore s'exécuter) elle-même dès sa création.
Pour auto-invoquer une fonction anonyme, il faut qu'elle soit inscrite elle-même entre parenthèses et suivi d'un couple de parenthèses appelantes.

Exemple :

        (function() {
                console.log("Je suis une fonction anonyme auto-invoquée");
        })();

> une fonction auto-invoquée s'exécutera toujours juste après sa déclaration et ne pourra pas être rappelée plus tard dans le script qui la contient.

### 2.4 Exécuter une fonction anonyme lors du déclenchement d'un évènement 

Il est enfin possible également de rattacher une fonction anonyme à ce qu'on appelle des **"gestionnaires d'évènements"** en JavaScript.

Le langage JavaScript permet de répondre à des évènements, c'est à dire d'exécuter une séquence de code lorsqu'un évènement survient.

Le JavaScript permet de répondre à de nombreux types d'évènements tels que un clic sur un élément de la page, l'appui sur une touche du clavier, le chargement d'une nouvelle page ...

Pour définir la réponse à un évènement en particulier, on utilise un gestionnaire d'évènement qui est une fonction prévue pour exécuter le code voulu lorsque l'évènement en question se produit.

Dans ce contexte, on pourra passer une fonction anonyme à un gestionnaire d'évènement pour qu'elle soit exécutée lors du déclenchement de l'évènement que ce gestionnaire prend en charge.

> L'utilisation pratique des fonctions anonymes avec les gestionnaires d'évènements sera vue plus tard au cours de la formation

## Exercices 

### Exercice 1 \: calcul du temps de parcours d'un trajet

On souhaite calculer le temps de parcours d'un trajet en voiture. Pour cela, on fournit au programme la vitesse moyenne et la distance à parcourir, ensuite le programme calcule et affiche le temps nécessaire en secondes.
On définit les unités à respecter suivantes :

* vitesse :     km/h
* distance :    km
* temps :       s

Exemple :

![Img_Parcours_1](img/Images_Chapitre_4/Img_Parcours_1.png)

Codez le programme de manière à ce que le calcul du temps de parcours soit réalisé par une fonction <code>calculerTempsParcoursSec()</code>. Cette fonction recevra en paramètres la vitesse et la distance et retournera la valeur du temps en secondes.

> **Attention** :  le temps sera affiché depuis le programme principal et non depuis la fonction 

> **Note** : vous pourrez utiliser la fonction <code>Math.floor(x)</code> qui renvoie le plus grand entier qui est inférieur ou égal à x 

Tester et validez ce programme

On souhaite maintenant afficher le temps de parcours en Heures/Minutes/Secondes (on négligera les 1/10ème de seconde).

Exemple :

![Img_Parcours_2](img/Images_Chapitre_4/Img_Parcours_2.png)

Modifiez et complétez le programme de manière à ce que la durée exprimée en Heures/Minutes/Secondes soit calculée et retournée sous forme d'une chaine de caractères par une fonction <code>convertir_h_min_sec()</code> qui recevra en paramètre le temps en secondes.  

> **Attention** :  le temps sera affiché depuis le programme principal et non depuis la fonction 

Tester et validez ce programme

### Exercice 2 \: recherche du nombre de multiples de 3

On souhaite afficher la liste des valeurs multiples de 3 comprises entre 0 et un nombre entier au choix. 

Exemple :

![Img_Multiples_3](img/Images_Chapitre_4/Img_Multiples_3.png)

Codez le programme de manière à ce que la recherche des multiples entre 0 et le nombre choisi soit réalisée par une fonction <code>rechercher_Mult3()</code>. Cette fonction recevra en paramètre le nombre choisi et retournera une chaine de caractères correspondant à la liste des valeurs trouvées.

>**Attention** : La liste des valeurs sera affichée depuis le programme principal et non depuis la fonction

Tester et validez ce programme