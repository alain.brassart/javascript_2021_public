# Les chaines de caractères en JavaScript et l'objet global String

> * Auteur : Alain BRASSART
> * Date de publication : 03/05/2021
> * OS: Windows 10 (version 20H2)
> * VS Code : version 1.55.2 (system setup)
> * Chrome : version 87.0.4280.141

![CC-BY-NC-SA](img/cc-by-nc-sa-150.png) [Licence Creative Commons](https://creativecommons.org/licenses/by-nc-sa/3.0/fr/)

## Sommaire

  - [1. Rappel](#1-rappel)
  - [2. Propriétés et méthodes de l'objet String](#2-propriétés-et-méthodes-de-lobjet-string)
    - [2.1 La propriété <code>length</code>](#21-la-propriété-length)
    - [2.2 La concaténation avec l'opérateur +](#22-la-concaténation-avec-lopérateur-)
    - [2.3 Récupérer un caractère avec <code>charAt()</code>](#23-récupérer-un-caractère-avec-charat)
    - [2.4 Rechercher une expression avec <code>indexOf()</code> et <code>lastIndexOf()</code>](#24-rechercher-une-expression-avec-indexof-et-lastindexof)
    - [2.5 Extraire une chaine avec <code>substring()</code> et <code>slice()</code>](#25-extraire-une-chaine-avec-substring-et-slice)
    - [2.6 Remplacer une expression avec <code>replace()</code>](#26-remplacer-une-expression-avec-replace)
    - [2.7 Vérifier l'existence d'une expression avec la méthode <code>includes()</code>](#27-vérifier-lexistence-dune-expression-avec-la-méthode-includes)
    - [2.8 Vérifier qu'une chaine commence ou se termine par une expression particulière avec les méthodes <code>startsWith()</code> et <code>endWith()</code>](#28-vérifier-quune-chaine-commence-ou-se-termine-par-une-expression-particulière-avec-les-méthodes-startswith-et-endwith)
    - [2.9 Découper une chaine de caractères avec la méthode <code>split()</code>](#29-découper-une-chaine-de-caractères-avec-la-méthode-split)
  - [3. Les fonctions de conversion <code>parseInt()</code> et <code>parseFloat()</code>](#3-les-fonctions-de-conversion-parseint-et-parsefloat) 
    - [3.1 La fonction <code>parseInt()</code>](#31-la-fonction-parseint) 
    - [3.2 La fonction <code>parseFloat()</code>](#32-la-fonction-parsefloat)
  - [Références MDN](#références-mdn)
  - [Activité \: décodage de trame GPS](#activité-décodage-de-trame-gps)

## 1. Rappel 

En JavaScript, les chaines de caractères peuvent être manipulées à l'aide des méthodes de l'objet global **String**

Pour déclarer une chaine de caractères, on peut utiliser les guillemets("") ou les apostrophes ('')

        let chaine1 = "Bonjour";
        let chaine2 = 'les SNIR' ;

Dans l'exemple ci-dessus, les variables <code>chaine1</code> et <code>chaine2</code> peuvent être assimilés à  des objets de type <code>String</code> sur lesquels on peut appliquer les propriétés et les méthodes définies pour ce type d'objet.

## 2. Propriétés et méthodes de l'objet String

### 2.1 La propriété <code>length</code>

Comme pour <code>Array</code>, l'objet <code>String</code> ne possède qu'une seule propriété, <code>length</code> qui retourne le nombre de caractères contenus dans une chaine.

Exemple :

        let chaine = "azerty";
        let tailleChaine = chaine.length;

Après l' exécution de ce script, la variable <code>tailleChaine</code> contient le nombre de caractères de la chaine de caractères contenue dans la variable <code>chaine</code>, ici 6 caractères

Aperçu dans la console du navigateur :

![Img_String_length](img/Images_Chapitre_8/Img_Sting_length.png)

### 2.2 La concaténation avec l'opérateur +

La concaténation est une opération qui consiste à assembler deux chaines en une seule, elle s'effectue à l'aide de l'opérateur +

Exemple :

        let chaine1 = "Bonjour ";
        let chaine2 = "les SNIR";
        let chaine = chaine1 + chaine2;

La variable <code>chaine</code> contient après l'exécution de ce script <code>"Bonjour les SNIR"</code>.

Aperçu dans la console du navigateur :

![Img_String_Concat_1](img/Images_Chapitre_8/Img_String_Concat_1.png)

>Attention à ne pas confondre l'opérateur de concaténation avec l'opérateur d'addition qui s'applique aux nombres

Exemple :

        let chaine1 = "2";
        let chaine2 = "3";
        let chaine = chaine1 + chaine2;

La variable <code>chaine</code> contient après l'exécution de ce script <code>"23"</code> et non pas le nombre 5

Aperçu dans la console du navigateur :

![Img_String_Concat_2](img/Images_Chapitre_8/Img_String_Concat_2.png)

Il est également possible d'utiliser l'opérateur <code>+=</code> pour ajouter du texte à une chaine déjà existante.

Exemple :

        let chaine1 = "Bonjour ";
        chaine1 += "les SNIR";

La variable <code>chaine1</code> contient après l'exécution de ce script <code>"Bonjour les SNIR"</code>.

### 2.3 Récupérer un caractère avec <code>charAt()</code>

La méthode <code>charAt()</code>permet de récupérer un caractère de la chaine sur laquelle elle s'applique. Le caractère retourné est celui dont la position est indiquée en argument.

> Attention la position du premier caractère correspond à l'indice 0.

Exemple :

        let chaine = "azerty";
        let car = chaine.charAt(1);

Après l'exécution de ce script, la variable <code>car</code> contient le caractère <code>"z"</code>

Aperçu dans la console du navigateur :

![Img_String_charAt](img/Images_Chapitre_8/Img_String_charAt.png)

> L'indice fourni doit être un nombre compris entre <code>0</code> et <code>chaine.length-1</code>, dans le cas contraire, si l'indice fourni est en dehors de cet intervalle, la méthode <code>charAt()</code> renverra une chaine vide, et si aucun indice n'est fourni, la valeur par défaut utilisée sera 0.

### 2.4 Rechercher une expression avec <code>indexOf()</code> et <code>lastIndexOf()</code>

La méthode <code>indexOf()</code> permet de récupérer la position de la première occurence d'un caractère ou d'une chaine de caractères contenu dans la chaine sur laquelle elle s'applique.

Cette méthode recherche l’expression passée en argument dans la chaine de caractères et renvoie la position à laquelle cette expression a été trouvée la première fois ou la valeur -1 si l’expression n’a pas été trouvée. 

Exemple :

        let chaine = "azerty";
        let index = chaine.indexOf("z");

Après l'exécution de ce script, la variable <code>index</code> contiendra la valeur 1 

Aperçu dans la console du navigateur :

![Img_String_indexOf_1](img/Images_Chapitre_8/Img_String_indexOf_1.png)

> Si l'expression recherchée est une chaine de caractères, la méthode <code>indexOf()</code> retourne l'index correspondant à la position du premier caractère

Exemple :

        let chaine = "azerty";
        let index = chaine.indexOf("rty");        

Après l'exécution de ce script, la variable <code>index</code> contiendra la valeur 3

Aperçu dans la console du navigateur :

![Img_String_indexOf_2](img/Images_Chapitre_8/Img_String_indexOf_2.png)

Par défaut, la recherche s'effectue dans toute la chaine, mais il est possible de passer un deuxième paramètre à la méthode <code>indexOf()</code> qui permet de choisir la position à partir de laquelle on souhaite démarrer la recherche.

Exemple :

        let chaine = "azerty ou qwerty";
        let index = chaine.indexOf("rty",4);

Après l'exécution de ce script, la variable <code>index</code> contiendra la valeur 13. En effet ici, la recherche de l'expression "rty"  se fait à partir du 5ème caractère de la chaine. Ce caractère correspondant à la lettre "t" du mot "azerty", la première occurence de l'expression "rty" contenue dans ce mot  ne sera donc pas trouvée mais elle le sera dans le mot "qwerty".

Aperçu dans la console du navigateur :

![Img_String_indexOf_3](img/Images_Chapitre_8/Img_String_indexOf_3.png)

La méthode <code>lastIndexOf()</code> va fonctionner de la même manière que <code>indexOf()</code> à la différence près qu'elle va renvoyer la position de la dernière occurence correspondant à l'expression recherchée (ou -1 si l'expression n'est pas trouvée)

### 2.5 Extraire une chaine avec <code>substring()</code> et <code>slice()</code>

La méthode <code>substring()</code> retourne une partie de la chaîne courante, comprise entre un indice de début et un indice de fin. Le caractère dont la position correspond à l'indice de fin n'est pas retourné.

Exemple :

        let chaine = "azerty";
        let ss_chaine = chaine.substring(1,3);

Après l'exécution de ce script, la variable <code>ss_chaine</code> contient la chaine de caractères <code>"ze"</code>

Aperçu dans le navigateur :

![img_String_substring_1](img/Images_Chapitre_8/Img_String_substring_1.png)

> Si les indices de début et de fin ont la même valeur <code>substring()</code> retourne une chaine vide.

        let chaine ="azerty";
        let ss_chaine = chaine.substring(1,1);  

Après l'exécution de ce script, la variable <code>ss_chaine</code> contient une chaine de caractères vide <code>""</code>

Aperçu dans le navigateur :

![img_String_substring_2](img/Images_Chapitre_8/Img_String_substring_2.png)

> Si l'indice de fin n'est pas spécifié, <code>substring()</code> effectuera l'extraction des caractères jusqu'à la fin de la chaine.

        let chaine ="azerty";
        let ss_chaine = chaine.substring(3); 

Après l'exécution de ce script, la variable <code>ss_chaine</code> contient la chaine de caractères <code>"rty"</code>

Aperçu dans le navigateur :

![img_String_substring_3](img/Images_Chapitre_8/Img_String_substring_3.png)

>Si l'un des indices est plus grand que <code>chaine.length</code>, il sera traité comme <code>chaine.length</code>

        let chaine ="azerty";
        let ss_chaine = chaine.substring(1,7);

Après l'exécution de ce script, la variable <code>ss_chaine</code> contient la chaine de caractères <code>"zerty"</code>

Aperçu dans le navigateur :

![img_String_substring_4](img/Images_Chapitre_8/Img_String_substring_4.png)

> Si les indices sont inversés, <code>substring()</code> rétablit l'ordre logique afin de les traiter comme si ils avaient été passés dans le bon ordre.

        let chaine ="azerty";
        let ss_chaine = chaine.substring(3,1);

Après l'exécution de ce script, la variable <code>ss_chaine</code> contient la chaine de caractères <code>"ze"</code>

Aperçu dans le navigateur :

![img_String_substring_5](img/Images_Chapitre_8/Img_String_substring_5.png)

<code>slice()</code> est une méthode d'extraction qui ressemble fortement à <code>substring()</code> mais avec une option en plus. Si une valeur négative est transmise pour la position de fin, <code>slice()</code> va extraire la chaine jusqu'à la fin, en décomptant le nombre de caractères correspondant.

Exemple :

        let chaine = "azerty";
        let ss_chaine = chaine.slice(0,-3);

Après l'exécution de ce script, la variable <code>ss_chaine</code> contient la chaine de caractères <code>"aze"</code>

L'utilisation de <code>substring()</code> dans les mêmes conditions donnerait en retour une chaine vide car toute valeur d'indice négative est remplacée par 0 pour cette fonction.

D'autre part, contrairement à <code>substring()</code>, <code>slice()</code> ne change pas l'ordre des indices, si l'indicde de fin est inférieur à celui du début, <code>slice()</code> retournera une chaine vide.

### 2.6 Remplacer une expression avec <code>replace()</code>

La méthode <code>replace()</code> recherche une expression dans une chaine de caractères et la remplace par une autre. Elle utilise deux paramètres :

* le premier correspond à l'expression qui doit être recherchée et remplacée
* le second correspond à l'expression qui doit être utilisée pour le remplacement

La méthode <code>replace()</code> retourne une nouvelle chaine de caractères qui contient les modifications, la chaine de départ n'est pas modifiée.
  
Exemple :

        let chaine1 = "azerty";
        let chaine2 = chaine1.replace("az", "qw");
  
Après l'exécution de ce script, la variable <code>chaine2</code> contient la chaine de caractères <code>"qwerty"</code>

Aperçu dans le navigateur :

![img_String_replace](img/Images_Chapitre_8/Img_String_replace.png)

> Dans le cas où l'expression à rechercher est une chaine de caractères, seule la première occurence de cette expression est remplacée, pour remplacer toutes les occurences, il faut passer par une expression régulière. (Cette partie sera vue plus tard)

### 2.7 Vérifier l'existence d'une expression avec la méthode <code>includes()</code>

La méthode <code>includes()</code> permet de déterminer si une chaine de caractères est inclue dans une autre. Si la chaine recherchée a été trouvée, <code>includes()</code> retourne la valeur <code>true</code>.

Exemple :

        let chaine = "azerty" ;
        let verif = chaine.includes("er");
        
        // verif vaut true car l'expression "er" est présente dans la chaine "azerty"

        let verif = chaine.includes("et");

        // verif vaut false car l'expression "et" n'est pas présente dans la chaine "azerty"

Par défaut, <code>includes()</code> commence la recherche de à partir du premier caractère (indice 0) et la chaine est parcourue dans sa totalité. Cependant, il est possible d'ajouter un second paramètre pour spécifier le rang à partir duquel on souhaite commencer la recherche.

Exemple :

        let chaine = "azerty" ;
        let verif = chaine.includes("er",1);
        
        // verif vaut toujours true car l'expression "er" est toujours présente dans la partie de la chaine qui commence à partir du caractère de rang 1 "zerty"

        let verif = chaine.includes("er",3);

        // verif vaut false car l'expression "er" n'est plus présente dans la partie de la chaine qui commence à partir du caractère de rang 3 "rty"

### 2.8 Vérifier qu'une chaine commence ou se termine par une expression particulière avec les méthodes <code>startWith()</code> et <code>endWith()</code>

La méthode <code>startWith()</code> permet de déterminer si une chaine de caractères commence par une expression en particulier. Dans ce cas, <code>startWith()</code> retourne la valeur <code>true</code>.

Exemple :

        let chaine = "azerty" ;
        let verif = chaine.startWith("az");
        
        // verif vaut true car la chaine "azerty" commence bien par l'expression "az" 

        let verif = chaine.includes("ty");

        // verif vaut false car la chaine "azerty" ne commence pas par l'expression "ty"

La méthode <code>endWith()</code> permet de déterminer si une chaine de caractères se termine par une expression en particulier. Dans ce cas, <code>endWith()</code> retourne la valeur <code>true</code>.

Exemple :

        let chaine = "azerty" ;
        let verif = chaine.endWith("ty");
        
        // verif vaut true car la chaine "azerty" se termine bien par l'expression "ty" 

        let verif = chaine.includes("az");

        // verif vaut false car la chaine "azerty" ne se termine pas par l'expression "az"

### 2.9 Découper une chaine de caractères avec la méthode <code>split()</code>

La méthode <code>split()</code> découpe une chaine de caractères en plusieurs sous-chaines à l'aide d'un séparateur et retourne un tableau contenant l'ensemble de ces sous-chaines. Cette méthode nécessite un paramètre qui représente le séparateur utilisé.

Exemple :

        let chaine = "azerty et qwerty";
        let tabMots = chaine.split(" ");   // le séparateur utilisé ici est le caractère correspondant à l'espace

        // tabMots est un tableau qui contient les 3 sous-chaines ["azerty","et","qwerty"];

## 3. Les fonctions de conversion <code>parseInt()</code> et <code>parseFloat()</code>

<code>parseInt()</code>et <code>parseFloat()</code> sont des fonctions globales de JavaScript, elles sont disponibles au plus haut niveau de l'environnement JavaScript et peuvent être appelées directement sans passer par un objet.

### 3.1 La fonction <code>parseInt()</code>

La fonction <code>parseInt()</code> permet de convertir une chaine de caractères en un nombre entier si cela est possible, cette fonction utilise 2 paramètres :

* la chaine de caractère qui sera analysée pour effectuer la conversion
* un nomnre entier qui représente la base utilisée pour calculer la valeur qui devra être retournée.

Exemple :

        let chaineNum = "12";
        let valNum_10 = parseInt(chaineNum,10);
        let valNum_12 = parseInt(chaineNum,16);

        // valNum_10 contient un nombre entier qui vaut 12
        // valNum_12 contient un nombre entier qui vaut 18 car parseInt calcule 12 en base 16

> Si le premier caractère de la chaine ne permet pas d'obtenir un nombre d'après la base fournie, <code>parseInt()</code> renvoie la valeur NaN (Not a Number)

Exemple :

        let chaineNum = "A3";
        let valNum_10 = parseInt(chaineNum,10);
        let valNum_12 = parseInt(chaineNum,16);

        // valNum_10 contient la valeur Nan car le caractère "A" ne correspond pas à un chiffre en base 10 
        // valNum_12 contient un nombre entier qui vaut 163 car le caractère "A" correspond à un chiffre en base 16  

> Si, lors de l'analyse de la chaine, <code>parseInt()</code> rencontre un caractère qui n'est pas un chiffre dans la base donnée, ce caractère ainsi que les suivants seront ignorés.

Exemple :

        let chaineNum = "3AFJ";
        let valNum_10 = parseInt(chaineNum,10);
        let valNum_12 = parseInt(chaineNum,16);

        // valNum_10 contient la valeur 3 car parseInt() ignore le caractère "A" et les suivants 
        // valNum_12 contient un nombre entier qui vaut 943 car cette fois-ci les trois premiers caractères de la chaine représentent bien des chiffres en base 16 

### 3.2 La fonction <code>parseFloat()</code>

La fonction <code>parseFloat()</code> permet de convertir une chaine de caractères en un nombre flottant si cela est possible. 

Exemple :

        let chaineNum = "23.58";
        let valNum = parseFloat(chaineNum);

        // valNum contient le nombre flottant 23.58        

> **Attention** pour cette fonction, le séparateur décimal qui convient est le point et non la virgule

Exemple :

        let chaineNum = "23,58";
        let valNum = parseFloat(chaineNum);

        // valNum contient le nombre 23 car la virgule n'est pas reconnue comme séparateur décimal    

> Par contre, la notation avec un exposant (à l'aide du caractère 'e'ou 'E')est interprétée correctement par <code>parseFloat()</code>

Exemple :

        let chaineNum = "2358e-2";
        let valNum = parseFloat(chaineNum);

        // valNum contient le nombre 23.58   

> Si, lors de l'analyse de la chaine, <code>parseFloat()</code> rencontre un caractère qui n'est pas un chiffre, un point, un exposant ou le signe + ou - , ce caractère ainsi que les suivants seront ignorés.

Exemple :

        let chaineNum = "23.5v85";
        let valNum = parseFloat(chaineNum);

        // valNum contient le nombre 23.5 car parseFloat() ignore le caractère "v" et les suivants     

## Références MDN

* Les chaines de caractères en JavScript : <br/>
<https://developer.mozilla.org/fr/docs/Learn/JavaScript/First_steps/Strings> 

* Formatage de texte : <br/>
<https://developer.mozilla.org/fr/docs/Web/JavaScript/Guide/Text_formatting> 

* Référence JavaScript sur l'objet String : <br/>
<https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Global_Objects/String>  

## Activité \: décodage de trame GPS

Le récepteur de position GPS fournit des trames ASCII au format NMEA 0183 (trames de type RMC). Ce type de trame est décrit en [annexe](annexes/Trames_GPS_Anx.pdf). Elle contient les informations de position mais aussi de date et d’heure en temps universel (temps UTC). Elle peut être stockée sous la forme d'une chaine de caractères pour pouvoir être ensuite décodée par une application.
        
Exemple de trame :

"$GPRMC,161229.487,A,3723.2475,N,12158.3416,W,0.13,309.62,120598,,*10\r\n”

Créez un nouveau projet html5 dans VS Code incluant un fichier JavaScript :

* nom du dossier racine : **Decodage_Trame_GPS**
* nom du fichier js : **decodage_trame_gps.js**

Pour traiter le contenu de la trame, on envisage d'utiliser une classe <code>Decodage_Trame_GPS</code> qui sera développée progressivement au cours de l'activité.

### Déclarations et initialisations

Déclarez la classe <code>Decodage_Trame_GPS</code> avec une propriété <code>trame</code> qui sera initialisée par le constructeur et une méthode <code>decoder()</code> qui sera vide pour l'instant.

Déclarez une variable <code>trame_gps</code> et initialisez cette variable avec la chaine de caractères correspondant à l'exemple donné dans l'énoncé.

Créez un objet <code>objDecodage_Trame_GPS</code> de la classe <code>Decodage_Trame_GPS</code> et dont la propriété <code>trame</code> sera initialisée avec la variable <code>trame_gps</code>

Vérifiez dans la console du navigateur que l'objet <code>objDecodage_Trame_GPS</code> a bien été instancié et que sa propriété <code>trame</code> a bien été initialisée correctement.

Résultat attendu :

![Img_Decodage_Trame_GPS_1](img/Images_Chapitre_8/Img_Decodage_Trame_GPS_1.png)

### Préparation de la trame au décodage

Avant de procéder au décodage proprement dit, on envisage de 'spliter' la trame afin de récupérer dans un tableau les différents champs qu'elle contient (voir leurs définitions en annexe)

Ajoutez dans la classe <code>Decodage_Trame_GPS</code> une nouvelle propriété <code>tabChamps</code> et utilisez cette propriété pour récupérer les différents champs de la trame.

Vérifiez dans la console du navigateur que l'objet <code>objDecodage_Trame_GPS</code> a bien été mis à jour et que sa propriété <code>tabChamps</code> contient bien les différents champs de la trame.

Résultat attendu :

![Img_Decodage_Trame_GPS_2](img/Images_Chapitre_8/Img_Decodage_Trame_GPS_2.png)

### Vérification de la validité de la trame

On commence le décodage de la trame en vérifiant sa validité. Pour cela, on prévoit d'utiliser une propriété de type Boolean <code>est_RMC</code> qui aura la valeur <code>false</code> par défaut. La valeur de cette propriété sera mise à jour et retournée par la méthode <code>decoder()</code>. 

La vérification de la validité de la trame se fera dans la méthode <code>decoder()</code> à l'aide d'une fonction interne <code>verifier_RMC()</code> qui devra renvoyer <code>true</code> si la trame présente dans la propriété <code>trame</code> est une trame complète (test sur le nombre de caractères) et qu'elle est de type RMC (elle débute par $GPRMC).

> Attention, on rappelle qu'une fonction interne n'a pas accés directement aux propriétés définies dans le constructeur de la classe. Il faudra donc passer par des variables locales qui seront déclarées dans la méthode qui la contient. (la méthode <code>decoder()</code> dans le cas présent)

Ajoutez et initialisez la propriété <code>est_RMC</code> avec sa valeur par défaut.

Déclarez et codez la fonction interne <code>verifierRMC()</code>

Complétez la méthode <code>decoder()</code> avec les instructions qui permettent de mettre à jour la valeur de propriété <code>est_RMC</code> et de la retourner.

Vérifiez alors que la méthode <code>decoder()</code> renvoie :

* true si la trame correspond bien à une trame RMC de bonne longueur
* false si la trame est d'un autre type mais de bonne longueur
* false si la trame est de type RMC mais incorrecte (il manque le dernier octet par exemple ou il y a un octet en trop).

### Extraction des informations de position latitude et longitude

Si la trame est valide, on peut en extraire les informations utiles comme la latitude et la longitude. Pour chacune de ces informations, la méthode <code>decoder()</code> devra mettre à jour les propriétés suivantes :

* pour la latitude :

  * la propriété <code>nbDegreLat</code> nombre entier qui représente la partie degrés de la latitude 
  * la propriété <code>nbMinLat</code> nombre décimal qui représente la partie minutes de la latitude
  * la propriété <code>latNS</code> caractère qui représente l'initiale de l'hémisphère terrestre. N pour Nord et S pour Sud

* pour la longitude :
  
  * la propriété <code>nbDegreLong</code> nombre entier qui représente la partie degrés de la longitude 
  * la propriété <code>nbMinLong</code> nombre décimal qui représente la partie minutes de la longitude
  * la propriété <code>longEW</code> caractère E pour désigner une longitude Est et W pour désigner une longitude Ouest

Pour extraire les informations de position contenues dans la trame et mettre à jour ces propriétés la méthode <code>decoder()</code> fera appel à une fonction interne <code>extrairePosition()</code>. 

> Comme pour la fonction <code>verifierRMC()</code>, la fonction <code>extrairePosition()</code> n'a pas accés directement aux propriétés définies dans le constructeur de la classe. Il faut donc passer par des variables locales qui seront déclarées dans la méthode <ode>decoder()</code>

Ajoutez et initialisez les 6 nouvelles propriétés avec leurs valeurs par défaut (0 pour les propriétés de type number et chaine vide pour les propriétés de type string) 

Déclarez et codez la fonction interne <code>extrairePosition()</code>. Cette fonction doit extraire les informations de position (latitude et longitude) depuis la chaine contenue dans la propriété <code>trame</code> et fournir à la méthode <code>decoder()</code> les valeurs qui devront être attribuées à chacune des propriétés définies ci-dessus    

Sachant que le décodage des informations ne doit être exécuté que si la trame est valide, complétez la méthode <code>decoder()</code> avec les instructions qui permettent de mettre à jour les valeurs des propriétés de latitude et de longitude.

Vérifiez dans la console du navigateur que la méthode <code>decoder()</code> a bien mis à jour les propriétés relatives à la latitude et la longitude.

Résultat attendu :

![Img_Decodage_Trame_GPS_3](img/Images_Chapitre_8/Img_Decodage_Trame_GPS_3.png)

### Lecture de la latitude et de la longitude

On souhaite maintenant disposer dans la classe <code>Decodage_Trame_GPS</code> de deux méthodes <code>lireLatitude()</code> et <code>lireLongitude()</code> qui permettent de compiler et retourner les informations de latitude et de longitude selon les formats suivants : 

* pour la latitude : dd ° mm.mmmm ' N (ou S) 
* pour la longitude : dd ° mm.mmmm ' E (ou W)

Déclarez et codez les méthodes <code>lireLatitude()</code> et <code>lireLongitude()</code>

Codez les instructions qui permettent de tester ces méthodes

Résultat attendu :

![Img_Decodage_Trame_GPS_4](img/Images_Chapitre_8/Img_Decodage_Trame_GPS_4.png)

### Extraction des informations relatives à la date

En plus des informations de position, il faudra extraire les informations concernant la date. Pour chacune de ces informations, la méthode <code>decoder()</code> devra mettre à jour de nouvelles propriétés :

* la propriété <code>jour</code> nombre entier qui représente le numéro du jour dans le mois 
* la propriété <code>mois</code> chaine de caractères qui représente le mois en écriture abrégé (voir tableau ci-dessous) 
* la propriété <code>annee</code> nombre qui représente l'année ramenée entre 1950 et 2050

Tableau des mois en écriture abrégée :

|   Mois   | Ecriture abrégée |
|---------|----------------|
| Janvier | Janv.|
| Février | Fevr. |
| Mars | Mars |
| Avril | Avr. |
| Mai | Mai |
| Juin | Juin |
| Juillet | Juil. |
| Août |  Août |     
| Septembre | Sept.|
| Octobre | Oct.|
| Novembre | Nov.|
| Décembre | Dec.|

<br/>

Pour extraire la date contenue dans la trame et mettre à jour ces propriétés la méthode <code>decoder()</code> fera appel à une fonction interne <code>extraireDate()</code>.

Ajoutez et initialisez les propriétés <code>jour</code>, <code>mois</code>, et <code>annee</code> avec leurs valeurs par défaut (0 pour les propriétés de type number et chaine vide pour les propriétés de type string)

Déclarez et codez la fonction interne <code>extraireDate()</code>. Cette fonction doit extraire la date depuis la chaine contenue dans la propriété <code>trame</code> et fournir à la méthode <code>decoder()</code> les valeurs qui devront être attribuées à chacune des propriétés définies ci-dessus

Sachant que le décodage des informations ne doit être exécuté que si la trame est valide, complétez la méthode <code>decoder()</code> avec les instructions qui permettent de mettre à jour les valeurs des propriétés pour la date

Vérifiez dans la console du navigateur que la méthode <code>decoder()</code> a bien mis à jour les propriétés relatives à la date.

Résultat attendu :

![Img_Decodage_Trame_GPS_5](img/Images_Chapitre_8/Img_Decodage_Trame_GPS_5.png)

### Lecture de la date

Comme pour les informations concernant la latitude et la longitude, on souhaite disposer dans la classe <code>Decodage_Trame_GPS</code> d'une méthode <code>lireDate()</code> qui permet de compiler et retourner la date selon le format suivant : jour mois annee

Déclarez et codez la méthode <code>lireDate()</code>

Codez les instructions qui permettent de tester cette méthode

Résultat attendu :

![Img_Decodage_Trame_GPS_6](img/Images_Chapitre_8/Img_Decodage_Trame_GPS_6.png)

### Extraction des informations relatives à l'heure

Pour terminer le décodage, il faudra extraire les informations concernant l'heure. Pour chacune de ces informations, la méthode <code>decoder()</code> devra à nouveau mettre à jour des propriétés supplémentaires :

* la propriété <code>heure</code> nombre entier qui représente l'heure courante
* la propriété <code>minute</code> nombre entier qui représente les minutes 
* la propriété <code>seconde</code> nombre décimal qui représente les secondes

Pour extraire l'heure contenue dans la trame et mettre à jour ces propriétés la méthode <code>decoder()</code> fera appel à une fonction interne <code>extraireHeure()</code>.

Ajoutez et initialisez les propriétés <code>heure</code>, <code>minute</code>, et <code>seconde</code> avec leurs valeurs par défaut (ici à 0)

Déclarez et codez la fonction interne <code>extraireHeure()</code>. Cette fonction doit extraire l'heure depuis la chaine contenue dans la propriété <code>trame</code> et fournir à la méthode <code>decoder()</code> les valeurs qui devront être attribuées à chacune des propriétés définies ci-dessus

Sachant que le décodage des informations ne doit être exécuté que si la trame est valide, complétez la méthode <code>decoder()</code> avec les instructions qui permettent de mettre à jour les valeurs des propriétés pour l'heure

Vérifiez dans la console du navigateur que la méthode <code>decoder()</code> a bien mis à jour les propriétés relatives à l'heure.

Résultat attendu :

![Img_Decodage_Trame_GPS_7](img/Images_Chapitre_8/Img_Decodage_Trame_GPS_7.png)

### Lecture de l'heure

La classe <code>Decodage_Trame_GPS</code> devra enfin disposer d'une méthode <code>lireHeure()</code> qui permet de compiler et retourner l'heure selon le format suivant : hh H mm mn ss.sss s

Déclarez et codez la méthode <code>lireDate()</code>

Codez les instructions qui permettent de tester cette méthode

Résultat attendu :

![Img_Decodage_Trame_GPS_8](img/Images_Chapitre_8/Img_Decodage_Trame_GPS_8.png)








